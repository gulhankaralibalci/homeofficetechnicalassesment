package com.HomeOffice.pages;

import net.serenitybdd.core.steps.UIInteractionSteps;
import org.openqa.selenium.By;

public class MessageInfoPage extends UIInteractionSteps {

    static By MESSAGE= By.xpath("//*[contains(text(), 'the UK')]");

    // This pages gives the result for the inquiry process as a message

    public String get_message(){
        return $(MESSAGE).getText();
    }
}
