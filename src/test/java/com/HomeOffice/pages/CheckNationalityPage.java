package com.HomeOffice.pages;

import net.serenitybdd.core.pages.WebElementFacade;
import net.serenitybdd.core.steps.UIInteractionSteps;
import net.thucydides.core.annotations.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;


public class CheckNationalityPage extends UIInteractionSteps {

    static By NEXT_STEP= By.xpath("//*[text()='Next step']");
    @FindBy(id="response")
    WebElementFacade countryDropDown;

    //Second page in the row, in which user selects their nationality

    @Step("Select the nationality")
    public void select_nationality_as(String nationality) {
        countryDropDown.selectByVisibleText(nationality);
        $(NEXT_STEP).click();
    }
}
