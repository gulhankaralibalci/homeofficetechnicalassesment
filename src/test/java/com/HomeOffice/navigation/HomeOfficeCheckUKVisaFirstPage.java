package com.HomeOffice.navigation;

import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("https://www.gov.uk/check-uk-visa/")
public class HomeOfficeCheckUKVisaFirstPage extends PageObject {
// We set the starting webpage for the whole tests
}
